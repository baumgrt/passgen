#' @name  passgen
#' @title A Simple Random Password Generator
#' @export passgen
#'
#' @description
#'   This function provides a method to create a random alpha-numeric password.
#'   In order to improve the typing speed, the password characters are chosen
#'   alternately from keys assigned to the left hand and the right hand,
#'   respectively. The user can specify whether the password should start with
#'   right-hand or a left-hand character, whether it should be case-sensitive
#'   and whether character repetitions are allowed. Because the password should
#'   be independent of the chosen keyboard layout, the character set is
#'   restricted to alpha-numeric characters with the exception of \code{"y"} and
#'   \code{"z"}, which are only included upon user request (assuming the QWERTZ
#'   keyboard layout is used).
#'
#' @param n             The lenght of the generated password (number of
#'                        characters)
#' @param startleft     Specifies if the password should start with a left-hand
#'                        character
#' @param case          Determines if the password is case-sensitive
#' @param rep           Determines if the password should contain repeating
#'                          characters.
#' @param zy            Determines if the characters \code{"y"} and \code{"z"}
#'                          (and their upper-case equivalent if applicable) are
#'                          allowed.
#'
#' @return
#'   A character string containing the generated password is returned.
#'
#' @examples
#' passgen()
#'
#' passgen(16, startleft = FALSE, case = TRUE, rep = TRUE)
#'
#' @keywords utilities

passgen <- function(n = 8L, startleft = TRUE, case = FALSE, rep = FALSE, zy = FALSE) {

    stopifnot(is.logical(startleft),
              is.logical(case),
              is.logical(rep),
              is.logical(zy))

    ## Setting up the list of available characters, grouped by the hand (i. e.,
    ## left or right) used to press them.
    LTR <- list(
        left  = c('q', 'w', 'e', 'r', 't', 'a', 's', 'd', 'f', 'g', 'x', 'c',
                  'v', 'b', '1', '2', '3', '4', '5', '6'),
        right = c('u', 'i', 'o', 'p', 'h', 'j', 'k', 'l', 'n', 'm', '7', '8',
                  '9', '0'))

    ## Add 'y' and 'z' if we are sure to have a QWERTZ-layout keyboard.
    if (zy) {
        LTR$left  <- c(LTR$left,  'y')
        LTR$right <- c(LTR$right, 'z')
    }

    ## Add upper-case letters if we want to have a case-sensitive password.
    if (case) {
        LTR$left  <- c(LTR$left,
                       'Q', 'W', 'E', 'R', 'T', 'A', 'S', 'D', 'F', 'G', 'X',
                       'C', 'V', 'B')
        LTR$right <- c(LTR$right,
                       'U', 'I', 'O', 'P', 'H', 'J', 'K', 'L', 'N', 'M')

        if (zy) {
            LTR$left  <- c(LTR$left,  'Y')
            LTR$right <- c(LTR$right, 'Z')
        }
    }

    ## Stop if there are not sufficiently many unique characters to construct
    ## a password of the requested length.
    if (!rep) {
        MAXLEN <- 2L * min(sapply(LTR, length)) + as.integer(startleft)
        if (!(n %in% seq_len(MAXLEN))) {
            warning(
                'There are not enough characters to construct a password of length ',
                n, '.\n',  'Creating a password of length ', MAXLEN, ' instead.',
                call.      = FALSE,
                immediate. = TRUE
            )
            n <- MAXLEN
        }
    }

    ## Define the positions within the password at which the left-hand and
    ## right-hand characters are about to be put.
    if (startleft) {
        LEFT  <- seq.int(1L, n, 2L)
        RIGHT <- seq.int(2L, n, 2L)
    } else {
        LEFT  <- seq.int(2L, n, 2L)
        RIGHT <- seq.int(1L, n, 2L)
    }

    ## Initialize an empty password and fill in the characters.
    PWD        <- character(n)

    PWD[LEFT]  <- sample(x       = LTR$left,
                         size    = length(LEFT),
                         replace = rep)

    PWD[RIGHT] <- sample(x       = LTR$right,
                         size    = length(RIGHT),
                         replace = rep)

    ## Glue the characters together and return the password
    return(paste(PWD, sep = '', collapse = ''))
}
# vim: tw=80 ft=r
