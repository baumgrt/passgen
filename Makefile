ROPTS := --quiet --no-save --vanilla


all:	build

documentation:
	R ${ROPTS} -e 'library(devtools); devtools::document()'

build:	documentation
	R ${ROPTS} -e 'library(devtools); devtools::build()'

check:
	R ${ROPTS} -e 'library(devtools); devtools::check()'

clean:
	rm -rf man
	rm -f NAMESPACE

distclean:	clean
	rm -f ../passgen_*.tar.gz


.PHONY:	all documentation build check clean distclean
